// coding with nick

// Js Documents

// Table of contyent
// 1.  vars and inits
// 2.  Inits Menu
// 3.  Init Timer
// 4.  Init Favorite
// 5.  Init Isotope Filtering
// 6.  Init Slider



jQuery(document).ready(function($)
{
    "user strict";

    // 1. vars and Inits

    var mainSlider = $('.main_slider');
    var hamburger = $('.hamburger_container');
    var menu = $('.hamburger_menu');
    var menuActive = false;
    var hamburgerClose = $('.hamburger_close');
    var fsOverlay = $('.fs_menu_overlay');


    initFavorite();
    initIsotopeFiltering();
    initTimer();
    $('#login').click(function(){
		openLogin()
})

// 2.  Inits Menu


// 3.  Init Timer

function initTimer()
{
    if($('.timer').length)
    {

       // var target_date = new Date("Oct 19, 2012").getTime();


        var date = new Date();
        date.setDate(date.getDate() + 3);
        var target_date = date.getTime();



        // variables for time units
        var days, hours, minutes, seconds;
        var d = $('#day');
        var h = $('#hour');
        var m = $('#minute');
        var s = $('#second');

        setInterval(function ()
        {
            // find the amount of "seconds" between now and target

            var current_date = new Date().getTime();
            var seconds_left = (target_date - current_date) / 1000;

            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;

            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;

            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);

            // display result
            d.text(days);
            h.text(hours);
            m.text(minutes);
            s.text(seconds);

        }, 1000 );
    }
}


// 4.  Init Favorite
function initFavorite()
{
    if($('.favorite').length)
    {
        var favs = $('.favorite');

        favs.each(function()
        {
            var fav = $(this);
            var active = false;
            if(fav.hasClass('active'))
            {
                active = true;
            }
            fav.on('click', function()
            {
                if(active)
                {
                    fav.removeClass('active');
                    active = false;
                }
                else
                {
                    fav.addClass('active');
                    active = true;
                }
            });
        });
    }
}



// 5.  Init Isotope Filtering

function initIsotopeFiltering(){
    if($('.grid_sorting_button').length)
    {
        $('.grid_sorting_button').click(function()
        {
            $('.grid_sorting_button.active').removeClass('active');
            $(this).addClass('active');

            var selector = $(this).attr('data-filter');
            $('.product-grid').isotope({
                filter: selector,
                AnimationOption: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false
        });
    }
}


function openLogin() {
	
	var str = '<form>'
	    str += '<div class="form-group">'
	    str += '<label for="productCode">Email*</label>'
		str += '<input type="text" class="form-control" id="email">'
		str += '</div>'
	    str += '<div class="form-group">'
	    str += '<label for="productCode">Password*</label>'
		str += '<input type="password" class="form-control" id="password">'
		str += '</div>'
		str += '<br>'
		str += '<div class="form-group">'
		str += '<input type="button" class="form-control btn btn-info" id="Login" value="Login">'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><input type="button" class="btn btn-link" id="lupaPassword" value="Lupa Password"></center>'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><span>Belum memiliki akun?</span><input type="button" class="btn btn-link" id="daftar" value="Daftar"></center>'
		str += '</div>'
		str += '</form>'
	$('.modal-title').html('Login')
	$('.modal-body').html(str)
	$('#myButton').html('Simpan Data')
	$('#myModal').modal({backdrop: 'static', keyboard: false}) 
	$('.modal-footer').attr('style', 'visibility: hidden')
	$('#myModal').modal('show')
}




































});