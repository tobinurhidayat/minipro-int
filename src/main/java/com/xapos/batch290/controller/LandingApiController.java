package com.xapos.batch290.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.MenuRole;
import com.xapos.batch290.repository.MenuRoleRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class LandingApiController {

	@Autowired
	public MenuRoleRepository menuRoleRepository;
	
	@GetMapping("menu")
	public ResponseEntity<List<MenuRole>> getAllMenuRole() {
		try {
			List<MenuRole> menuRole = this.menuRoleRepository.findByIsDelete(false);
			return new ResponseEntity<>(menuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
}
