package com.xapos.batch290.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.BiodataModel;
import com.xapos.batch290.model.Blood;
import com.xapos.batch290.model.Customer;
import com.xapos.batch290.model.User;
import com.xapos.batch290.repository.BiodataRepository;
import com.xapos.batch290.repository.CustomerRepository;
import com.xapos.batch290.repository.UserRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ProfileApiController {

	@Autowired
	BiodataRepository biodataRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	CustomerRepository customerRepository;
	
	@GetMapping("profile")
	public ResponseEntity<List<Customer>> getAllProduct() {
		try {
			
			List<Customer> customer = this.customerRepository.findByIsDelete(false);
			return new ResponseEntity<>(customer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("data/")
	public ResponseEntity<List<BiodataModel>> data(@RequestParam String fullName) {
		try {
			List<BiodataModel> biodataModel = this.biodataRepository.findByFullName(fullName);
			return new ResponseEntity<>(biodataModel, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("email")
	public ResponseEntity<List<User>> getEmail(@RequestParam Long biodataId) {
		try {
			
			List<User> user = this.userRepository.findByBiodataId(biodataId);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
}
