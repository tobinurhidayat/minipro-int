package com.xapos.batch290.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/action/")
public class ActionController {

	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("action/indexapi");
		return view;
	}
	
	@GetMapping("layout")
	public ModelAndView layout() {
		ModelAndView view = new ModelAndView("fragment/layout");
		return view;
	}
}
