package com.xapos.batch290.controller;


import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.BiodataModel;
import com.xapos.batch290.model.Blood;
import com.xapos.batch290.repository.BiodataRepository;
import com.xapos.batch290.repository.BloodRepository;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class BloodApiController {

	@Autowired
	public BloodRepository bloodRepository;


	@GetMapping("blood")
	public ResponseEntity<List<Blood>> getAllProduct() {
		try {
			
			List<Blood> blood = this.bloodRepository.findByIsDelete(false);
			blood = blood.stream().sorted(Comparator.comparing(Blood::getCode)).collect(Collectors.toList());
			return new ResponseEntity<>(blood, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("getblood")
	public ResponseEntity<Object> getAll() {
		try {
		List<Blood> listdata = bloodRepository.findAll();
		List<Blood> listData = listdata.stream()
				/* .filter(s -> s.getIsDelete()== false) */
				.filter(s -> s.getCode().startsWith("A"))
				.sorted(Comparator.comparing(Blood::getCode)).collect(Collectors.toList());

		return new ResponseEntity<>(listData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/blood/{id}")
	public ResponseEntity<List<Blood>> getLocationLevelById(@PathVariable("id") Long id){
		try {
			Optional<Blood> blood = this.bloodRepository.findById(id);
			if(blood.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(blood,HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}catch(Exception e) {
			return new ResponseEntity<List<Blood>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/blood")
	public ResponseEntity<Object> saveAdd(@RequestBody Blood blood){
		Long user = new Long("1");
		blood.setCreatedBy(user);
		blood.setCreatedOn(Date.from(Instant.now()));
		blood.setIsDelete(false);
		Blood bloodData = this.bloodRepository.save(blood);
		if(bloodData.equals(blood)) {
			return new ResponseEntity<Object>("The data is succesfully saved.",HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("The data can not be saved.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("edit/blood/{id}")
	public ResponseEntity<Object> saveEdit(@PathVariable("id") Long id, @RequestBody Blood blood){
		
		Optional<Blood> bloodData = this.bloodRepository.findById(id);
		
		if(bloodData.isPresent()) {
			blood.setId(id);
			blood.setCreatedBy(bloodData.get().getCreatedBy());
			blood.setCreatedOn(bloodData.get().getCreatedOn());
			blood.setModifiedBy(id);
			blood.setModifiedOn(Date.from(Instant.now()));
			blood.setDeletedBy(bloodData.get().getDeletedBy());
			blood.setDeletedOn(bloodData.get().getDeletedOn());
			blood.setIsDelete(bloodData.get().getIsDelete());
			this.bloodRepository.save(blood);
			return new ResponseEntity<Object>("The data is successfully updated.",HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/blood/{id}")
	public ResponseEntity<Object> saveDelete(@PathVariable("id") Long id){
		Optional<Blood> bloodData = this.bloodRepository.findById(id);
		
		if(bloodData.isPresent()) {
			Long user = new Long("1");
			Blood blood = new Blood();
			blood.setId(id);
			blood.setCode(bloodData.get().getCode());
			blood.setDescription(bloodData.get().getDescription());
			blood.setIsDelete(true);
			blood.setCreatedBy(bloodData.get().getCreatedBy());
			blood.setCreatedOn(bloodData.get().getCreatedOn());
			blood.setDeletedBy(user);
			blood.setDeletedOn(Date.from(Instant.now()));
			blood.setModifiedBy(bloodData.get().getModifiedBy());
			blood.setModifiedOn(bloodData.get().getModifiedOn());
			this.bloodRepository.save(blood);
			return new ResponseEntity<>("The data is succesfully deleted.",HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("blood/search/")
	public ResponseEntity<List<Blood>> getCode(@RequestParam("keyword") String keyword) {
		if(keyword.equals("")) {
			List<Blood> blood = this.bloodRepository.findByIsDelete(false);
			return new ResponseEntity<List<Blood>>(blood, HttpStatus.OK);
		} else {
			List<Blood> blood = this.bloodRepository.searchKeyword(keyword);
			return new ResponseEntity<List<Blood>>(blood, HttpStatus.OK);
		}
	}
	
	@GetMapping("code")
	public ResponseEntity<List<Blood>> getAllBlood(@RequestParam("code") String code) {
		List<Blood> codes = this.bloodRepository.findByCode(code);
		return new ResponseEntity<List<Blood>>(codes,HttpStatus.OK);
	}
}
