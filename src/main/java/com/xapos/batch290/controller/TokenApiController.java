package com.xapos.batch290.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.BiodataModel;
import com.xapos.batch290.model.Token;
import com.xapos.batch290.repository.UserRepository;
import com.xapos.batch290.repository.TokenRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class TokenApiController {
	@Autowired
	public TokenRepository tokenRepository;
	
	@PostMapping("token")
	public ResponseEntity<Object> tokens(@RequestBody Token token){
		
		token.setUsedFor("Daftar Akun");
		token.setIsExpired(false);
		token.setIsDelete(false);
		token.setCreatedOn(Date.from(Instant.now()));
		Token tokend = this.tokenRepository.save(token);
		if(tokend.equals(token)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("gettoken")
	public ResponseEntity<List<Token>> getToken(@RequestParam("token") String token) {
		List<Token> tokens = this.tokenRepository.findByToken(token);
		return new ResponseEntity<List<Token>>(tokens,HttpStatus.OK);
	}
	
	@GetMapping("allToken/{id}")
	public ResponseEntity<List<Token>> getTokenById(@PathVariable("id") Long id){
		try {
			Optional<Token> tokens = this.tokenRepository.findById(id);
			if(tokens.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(tokens, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Token>>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping("alltoken")
	public ResponseEntity<List<Token>> getAllToken(){
		try {
			List<Token> token = this.tokenRepository.findByIsExpired(false);
			return new ResponseEntity<>(token, HttpStatus.OK);
					
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT); 
		}
	}
	
	@PutMapping("kirimulang/{id}")
	public ResponseEntity<Object> kirimUlang(@PathVariable("id") Long id, @RequestBody Token token) {
		Optional<Token> tokens = this.tokenRepository.findById(id);

		if (tokens.isPresent()) {
			token.setEmail(tokens.get().getEmail());
//			token.setModifyBy("user");
			token.setIsDelete(tokens.get().getIsDelete());
			token.setModifyOn(Date.from(Instant.now()));
			token.setCreatedBy(tokens.get().getCreatedBy());
			token.setCreatedOn(tokens.get().getCreatedOn());
			token.setExpiredOn(Date.from(Instant.now()));
			token.setId(id);
			token.setToken(tokens.get().getToken());
			token.setUsedFor(tokens.get().getUsedFor());
			this.tokenRepository.save(token);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}

