package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.Blood;
import com.xapos.batch290.model.User;
import com.xapos.batch290.repository.UserRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class UserApiController {

	@Autowired
	public UserRepository userRepository;
	
	@GetMapping("getemail")
	public ResponseEntity<List<User>> getEmail(@RequestParam("email") String email) {
		List<User> emails = this.userRepository.findByEmail(email);
		return new ResponseEntity<List<User>>(emails, HttpStatus.OK);
	}
	
	@PutMapping("edit/email/{id}")
	public ResponseEntity<Object> saveEdit(@PathVariable("id") Long id, @RequestBody User user){
		
		Optional<User> userData = this.userRepository.findById(id);
		
		if(userData.isPresent()) {
			user.setId(id);
			user.setBiodataId(userData.get().getBiodataId());
			user.setPassword(userData.get().getPassword());
			user.setCreatedBy(userData.get().getCreatedBy());
			user.setCreatedOn(userData.get().getCreatedOn());
			user.setModifyBy(id);
			user.setModifyOn(Date.from(Instant.now()));
			user.setDeletedBy(userData.get().getDeletedBy());
			user.setDeletedOn(userData.get().getDeletedOn());
			user.setIsDelete(userData.get().getIsDelete());
			this.userRepository.save(user);
			return new ResponseEntity<Object>("The data is successfully updated.",HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
