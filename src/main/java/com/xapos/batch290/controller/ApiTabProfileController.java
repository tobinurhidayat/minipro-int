package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.BiodataModel;
import com.xapos.batch290.model.Blood;
import com.xapos.batch290.model.Customer;
import com.xapos.batch290.model.Spesialisasi;
import com.xapos.batch290.model.TambahSpesialisasi;
import com.xapos.batch290.model.User;
import com.xapos.batch290.repository.BiodataRepository;
import com.xapos.batch290.repository.CustomerRepository;
import com.xapos.batch290.repository.SpesialisasiRepository;
import com.xapos.batch290.repository.TambahSpesialisasiRepository;
import com.xapos.batch290.repository.UserRepository;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiTabProfileController {

	@Autowired
	BiodataRepository biodataRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	SpesialisasiRepository spesialisasiRepository;
	@Autowired
	TambahSpesialisasiRepository tambahSpesialisasiRepository;
	
	@GetMapping("spesialisasi")
	public ResponseEntity<List<Spesialisasi>> getAllSpesialisasi(){
		try {
			List<Spesialisasi> spesial = this.spesialisasiRepository.findByIsDelete(false);
			return new ResponseEntity<>(spesial, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("spesials")
	public ResponseEntity<List<TambahSpesialisasi>> getAllSpesial(){
		try {
			List<TambahSpesialisasi> spesial = this.tambahSpesialisasiRepository.findByIsDelete(false);
			return new ResponseEntity<>(spesial, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	@PostMapping("add/spesial")
	public ResponseEntity<Object> saveAdd(@RequestBody TambahSpesialisasi tambahSpesialisasi){
		Long user = new Long("1");
		tambahSpesialisasi.setCreatedBy(user);
		tambahSpesialisasi.setCreatedOn(Date.from(Instant.now()));
		tambahSpesialisasi.setIsDelete(false);
		TambahSpesialisasi tambahSpesialData = this.tambahSpesialisasiRepository.save(tambahSpesialisasi);
		if(tambahSpesialData.equals(tambahSpesialisasi)) {
			return new ResponseEntity<Object>("The data is succesfully saved.",HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("The data can not be saved.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("spesials/{id}")
	public ResponseEntity<List<TambahSpesialisasi>> getSpesialsById(@PathVariable("id") Long id){
		try {
			Optional<TambahSpesialisasi> spesialss = this.tambahSpesialisasiRepository.findById(id);
			if(spesialss.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(spesialss, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<TambahSpesialisasi>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("spesialisasi/{id}")
	public ResponseEntity<List<Spesialisasi>> getSpesialisasiById(@PathVariable("id") Long id){
		try {
			Optional<Spesialisasi> spesialisasi = this.spesialisasiRepository.findById(id);
			if(spesialisasi.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(spesialisasi, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Spesialisasi>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/spesials/{id}")
	public ResponseEntity<Object> editSpesials(@PathVariable("id") Long id, @RequestBody TambahSpesialisasi spesial){
		Optional<TambahSpesialisasi> spesialData = this.tambahSpesialisasiRepository.findById(id);
		
		if(spesialData.isPresent()) {

			spesial.setId(id);
			spesial.setCreatedBy(spesialData.get().getCreatedBy());
			spesial.setCreatedOn(spesialData.get().getCreatedOn());
			spesial.setModifiedBy(id);
			spesial.setModifiedOn(Date.from(Instant.now()));
			spesial.setDeletedBy(spesialData.get().getDeletedBy());
			spesial.setDeletedOn(spesialData.get().getDeletedOn());
			spesial.setIsDelete(spesialData.get().getIsDelete());

			this.tambahSpesialisasiRepository.save(spesial);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("user")
	public ResponseEntity<List<User>> getAllUser(){
		try {
			List<User> user = this.userRepository.findByIsDelete(false);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("customer")
	public ResponseEntity<List<Customer>> getAllCustomer(){
		try {
			List<Customer> customer = this.customerRepository.findByIsDelete(false);
			return new ResponseEntity<>(customer, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("biodata")
	public ResponseEntity<List<BiodataModel>> getAllBiodata(){
		try {
			List<BiodataModel> biodata = this.biodataRepository.findByIsDelete(false);
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("biodata/{id}")
	public ResponseEntity<List<BiodataModel>> getBiodataById(@PathVariable("id") Long id){
		try {
			Optional<BiodataModel> biodata = this.biodataRepository.findById(id);
			if(biodata.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<BiodataModel>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("user/{id}")
	public ResponseEntity<List<User>> getUserById(@PathVariable("id") Long id){
		try {
			Optional<User> user = this.userRepository.findById(id);
			if(user.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(user, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("customer/{id}")
	public ResponseEntity<List<Customer>> getCustomerById(@PathVariable("id") Long id){
		try {
			Optional<Customer> custumer = this.customerRepository.findById(id);
			if(custumer.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(custumer, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/biodata/{id}")
	public ResponseEntity<Object> editBiodata(@PathVariable("id") Long id, @RequestBody BiodataModel biodata){
		Optional<BiodataModel> biodataData = this.biodataRepository.findById(id);
		
		if(biodataData.isPresent()) {
			Long user = new Long("1");
			biodata.setId(id);
			biodata.setModifiedBy(user);
			biodata.setModifiedOn(Date.from(Instant.now()));
			biodata.setIsDelete(biodataData.get().getIsDelete());
			biodata.setCreatedBy(biodataData.get().getCreatedBy());
			biodata.setCreatedOn(biodataData.get().getCreatedOn());
			biodata.setImage(biodataData.get().getImage());
			biodata.setImagePath(biodataData.get().getImagePath());
			
			this.biodataRepository.save(biodata);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("edit/customer/{id}")
	public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @RequestBody Customer customer){
		Optional<Customer> customerData = this.customerRepository.findById(id);
		
		if(customerData.isPresent()) {
			customer.setId(id);
			customer.setBiodataId(customerData.get().getBiodataId());
			Long modify = new Long("1");
			customer.setModifiedBy(modify);
			customer.setModifiedOn(Date.from(Instant.now()));
			customer.setIsDelete(customerData.get().getIsDelete());
			customer.setCreatedBy(customerData.get().getCreatedBy());
			customer.setCreatedOn(customerData.get().getCreatedOn());
			customer.setHeight(customerData.get().getHeight());
			customer.setGender(customerData.get().getGender());
			customer.setWeight(customerData.get().getWeight());
			
			this.customerRepository.save(customer);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping("password")
	public ResponseEntity<List<User>> getAllBlood(@RequestParam("password") String password) {
		List<User> pass = this.userRepository.findByPassword(password);
		return new ResponseEntity<List<User>>(pass,HttpStatus.OK);
	}
}
