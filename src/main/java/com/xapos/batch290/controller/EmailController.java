package com.xapos.batch290.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.Token;
import com.xapos.batch290.service.EmailService;

@RestController
public class EmailController {
	@Autowired private EmailService emailService;

	 
    // Sending a simple Email
    @PostMapping("/sendMail")
    public String
    sendMail(@RequestBody Token token)
    {
        String status
            = emailService.sendSimpleMail(token);
 
        return status;
    }
    
   

}
