package com.xapos.batch290.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ProfileController {

	@GetMapping("indexapi")
	public ModelAndView tabprofile() {
		ModelAndView view = new ModelAndView("profile/indexapi");
		return view;
	}
}
