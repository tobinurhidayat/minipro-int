package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos.batch290.model.Blood;
import com.xapos.batch290.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByIsDelete(Boolean isDelete);
	
	List<User> findByBiodataId(Long biodataId);
	
	@Query(value = "FROM User WHERE password =?1 AND  isDelete=false")
	List<User> findByPassword(String password);
	
	List<User> findByEmail(String email);
}
