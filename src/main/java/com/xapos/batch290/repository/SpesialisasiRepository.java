package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.Spesialisasi;

public interface SpesialisasiRepository extends JpaRepository<Spesialisasi, Long>{

	List<Spesialisasi> findByIsDelete(Boolean isDelete);
}
