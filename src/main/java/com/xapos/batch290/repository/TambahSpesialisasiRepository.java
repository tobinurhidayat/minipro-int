package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.TambahSpesialisasi;

public interface TambahSpesialisasiRepository extends JpaRepository<TambahSpesialisasi, Long>{

	List<TambahSpesialisasi> findByIsDelete(Boolean isDelete);
}
