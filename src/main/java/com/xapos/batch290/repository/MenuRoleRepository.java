package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.MenuRole;

public interface MenuRoleRepository extends JpaRepository<MenuRole, Long>{

	List<MenuRole> findByIsDelete(Boolean isDelete);
}
