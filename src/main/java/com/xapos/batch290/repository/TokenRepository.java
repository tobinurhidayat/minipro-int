package com.xapos.batch290.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos.batch290.model.Token;



public interface TokenRepository extends JpaRepository<Token, Long>{

	List<Token> findByToken(String token);
//	List<Token> findByEmail(String email);
	
	List<Token> findByIsExpired(Boolean isExpired);
	
	@Query(value="SELECT MAX(id) FROM Token")
	List<Token> findByMaxId(long id);

}
