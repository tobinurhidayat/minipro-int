package com.xapos.batch290.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.LocationLevelModel;



public interface LocationLevelRepository extends JpaRepository<LocationLevelModel, Long> {

}
